### GraceUI 官网
[https://www.graceui.com/](https://www.graceui.com/)

### Grace Animation Library
>Grace Animation Library ( 简写 ： GAL ) 是 GraceUI 团队为 uni-app 打造的一款 免费、开源 的动画库，GAL 动画技术底层为 animate.css 动画、基于 uni.createAnimation 接口实现的动画、js + css3 动画。我们为大家封装了极其丰富的动画效果组件，通过这些组件您可以方便的实现开发过程中所需的各类动画效果。
>
>注意 :  GAL 以 css3 及 js 为基础来实现动画效果，暂不支持 nvue。

### 演示程序
>微信小程序搜索 GraceUI 动画库
>![扫码体验](https://img-cdn-tc.dcloud.net.cn/uploads/article/20210429/c025d4963707d8a0455f2925198a6dd6.jpg)

### 版权及免责
>GLA 遵循 MIT 开源协议，您可以免费获取动画库并可以在项目中使用，无需任何费用。

### 一些期望
>01. 我们提供了基础的动画组件，您可以利用哈=基础组件组合出更多更丰富的动画效果；
>02. 遇到问题请邮件 至 43412103@qq.com;
>03. 提交常用动画和我们一起完善动画库，请邮件至 43412103@qq.com;


### 使用手册
[https://www.graceui.com/animate/info/10185-0.html](https://www.graceui.com/animate/info/10185-0.html)

